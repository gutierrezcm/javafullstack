package com.mitocode.javafullstack.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Table
@Entity(name = "detail_sale")
public class DetailSale {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer idDetaileSale;

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "id_sale", nullable = false, foreignKey = @ForeignKey(name = "FK_sale_detailSale"))
  private Sale sale;

  @ManyToOne
  @JoinColumn(name = "id_product", nullable = false, foreignKey = @ForeignKey(name = "FK_product_detailSale"))
  private Product product;

  @Column(name = "quantity", nullable = true)
  private Integer quantity;

  public Integer getIdDetaileSale() {
    return idDetaileSale;
  }

  public void setIdDetaileSale(Integer idDetaileSale) {
    this.idDetaileSale = idDetaileSale;
  }

  public Sale getSale() {
    return sale;
  }

  public void setSale(Sale sale) {
    this.sale = sale;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }
}
