package com.mitocode.javafullstack.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Table
@Entity(name = "sale")
public class Sale {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer idSale;

  @Column(name = "date")
  //@NotNull
  private LocalDateTime date;

  //@NotNull
  @ManyToOne
  @JoinColumn(name = "id_person", nullable = false, foreignKey = @ForeignKey(name = "FK_sale_person"))
  private Person person;

  //@NotNull
  @Column(name = "amount")
  private BigDecimal amount;

  @OneToMany(mappedBy = "sale", cascade = { CascadeType.ALL }, orphanRemoval = true)
  private List<DetailSale> detailsSale;

  public Integer getIdSale() {
    return idSale;
  }

  public void setIdSale(Integer idSale) {
    this.idSale = idSale;
  }

  public LocalDateTime getDate() {
    return date;
  }

  public void setDate(LocalDateTime date) {
    this.date = date;
  }

  public Person getPerson() {
    return person;
  }

  public void setPerson(Person person) {
    this.person = person;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public List<DetailSale> getDetailsSale() {
    return detailsSale;
  }

  public void setDetailsSale(List<DetailSale> detailsSale) {
    this.detailsSale = detailsSale;
  }
}
