package com.mitocode.javafullstack.model;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Table
@Entity(name = "product")
public class Product {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer idProduct;

  @Size(min = 3, message = "El nombre debe tener minimo 3 caracteres")
  @Column(name = "name", length = 80, nullable = true)
  private String name;

  @Size(min = 3, message = "El brandName debe tener minimo 3 caracteres")
  @Column(name = "brand_name", length = 80, nullable = true)
  private String brandName;

  public Integer getIdProduct() {
    return idProduct;
  }

  public void setIdProduct(Integer idProduct) {
    this.idProduct = idProduct;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getBrandName() {
    return brandName;
  }

  public void setBrandName(String brandName) {
    this.brandName = brandName;
  }
}
