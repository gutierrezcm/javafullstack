package com.mitocode.javafullstack.model;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Table
@Entity(name = "person")
public class Person {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer idPerson;

  @Size(min = 3, message = "Nombres debe tener minimo 3 caracteres")
  @Column(name = "names", length = 70, nullable = true)
  private String names;

  @Size(min = 3, message = "Apellidos debe tener minimo 3 caracteres")
  @Column(name = "lastNames", length = 70, nullable = true)
  private String lastNames;

  public Integer getIdPerson() {
    return idPerson;
  }

  public void setIdPerson(Integer idPerson) {
    this.idPerson = idPerson;
  }

  public String getNames() {
    return names;
  }

  public void setNames(String names) {
    this.names = names;
  }

  public String getLastNames() {
    return lastNames;
  }

  public void setLastNames(String lastNames) {
    this.lastNames = lastNames;
  }
}
