package com.mitocode.javafullstack.controller;

import com.mitocode.javafullstack.model.Sale;
import com.mitocode.javafullstack.service.ISaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/sales")
public class SaleRestController {

  @Autowired
  private ISaleService saleService;

  @PostMapping
  public ResponseEntity<Sale> registrar(@Valid @RequestBody Sale sale) {
    return new ResponseEntity<Sale>(saleService.save(sale), HttpStatus.CREATED);
  }

  @GetMapping
  public ResponseEntity<List<Sale>> list() {
    return new ResponseEntity<List<Sale>>(saleService.listAll(), HttpStatus.OK);
  }

}
