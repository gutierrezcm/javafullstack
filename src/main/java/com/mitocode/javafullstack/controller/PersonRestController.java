package com.mitocode.javafullstack.controller;

import com.mitocode.javafullstack.exception.ModelNotFoundException;
import com.mitocode.javafullstack.model.Person;
import com.mitocode.javafullstack.service.IPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/persons")
public class PersonRestController {

  @Autowired
  private IPersonService productService;

  @GetMapping
  public ResponseEntity<List<Person>> list() {
    List<Person> list = productService.listAll();
    return new ResponseEntity<List<Person>>(list, HttpStatus.OK);
  }

  @GetMapping("/{id}")
  public ResponseEntity<Person> findById(@PathVariable("id") Integer id) {
    Person obj = productService.findById(id);
    if (obj.getIdPerson() == null) {
        throw new ModelNotFoundException("ID NO ENCONTRADO " + id);
    }
    return new ResponseEntity<Person>(obj, HttpStatus.OK);
  }

    //https://docs.spring.io/spring-hateoas/docs/current/reference/html/
    //Hateoas 1.0 > Spring Boot 2.2
  @GetMapping("/hateoas/{id}")
  public EntityModel<Person> findByIdHateoas(@PathVariable("id") Integer id) {
    Person obj = productService.findById(id);

    //localhost:8080/paciente/{id}
    EntityModel<Person> resource = new EntityModel<>(obj);
    WebMvcLinkBuilder linkTo = linkTo(methodOn(this.getClass()).findById(id));
    resource.add(linkTo.withRel("person-resource"));
    return resource;
  }

  @PostMapping
  public ResponseEntity<Person> save(@Valid @RequestBody Person person) {
    return new ResponseEntity<Person>(productService.save(person), HttpStatus.CREATED);
  }

  @PutMapping
  public ResponseEntity<Person> modify(@RequestBody Person person) {
    return new ResponseEntity<Person>(productService.modify(person), HttpStatus.OK);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Object> deleteById(@PathVariable("id") Integer id) {
    productService.remove(id);
    return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
  }

}
