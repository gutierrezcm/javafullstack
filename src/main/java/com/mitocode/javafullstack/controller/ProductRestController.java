package com.mitocode.javafullstack.controller;

import com.mitocode.javafullstack.exception.ModelNotFoundException;
import com.mitocode.javafullstack.model.Product;
import com.mitocode.javafullstack.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/products")
public class ProductRestController {

  @Autowired
  private IProductService productService;

  @GetMapping
  public ResponseEntity<List<Product>> list() {
    List<Product> list = productService.listAll();
    return new ResponseEntity<List<Product>>(list, HttpStatus.OK);
  }

  @GetMapping("/{id}")
  public ResponseEntity<Product> findById(@PathVariable("id") Integer id) {
    Product obj = productService.findById(id);
    if (obj.getIdProduct() == null) {
      throw new ModelNotFoundException("ID NO ENCONTRADO " + id);
    }
    return new ResponseEntity<Product>(obj, HttpStatus.OK);
  }

  //https://docs.spring.io/spring-hateoas/docs/current/reference/html/
  //Hateoas 1.0 > Spring Boot 2.2
  @GetMapping("/hateoas/{id}")
  public EntityModel<Product> findByIdHateoas(@PathVariable("id") Integer id) {
    Product obj = productService.findById(id);

    //localhost:8080/paciente/{id}
    EntityModel<Product> resource = new EntityModel<>(obj);
    WebMvcLinkBuilder linkTo = linkTo(methodOn(this.getClass()).findById(id));
    resource.add(linkTo.withRel("product-resource"));
    return resource;
  }

  @PostMapping
  public ResponseEntity<Product> save(@Valid @RequestBody Product product) {
    return new ResponseEntity<Product>(productService.save(product), HttpStatus.CREATED);
  }

  @PutMapping
  public ResponseEntity<Product> modify(@RequestBody Product product) {
    return new ResponseEntity<Product>(productService.modify(product), HttpStatus.OK);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Object> deleteById(@PathVariable("id") Integer id) {
    productService.remove(id);
    return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
  }

}
