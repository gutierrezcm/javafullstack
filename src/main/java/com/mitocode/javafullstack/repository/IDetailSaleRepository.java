package com.mitocode.javafullstack.repository;

import com.mitocode.javafullstack.model.DetailSale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IDetailSaleRepository extends JpaRepository<DetailSale, Integer> {
}
