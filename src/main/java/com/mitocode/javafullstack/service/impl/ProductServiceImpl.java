package com.mitocode.javafullstack.service.impl;

import com.mitocode.javafullstack.model.Product;
import com.mitocode.javafullstack.repository.IProductRepository;
import com.mitocode.javafullstack.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProductServiceImpl implements IProductService {

  @Autowired
  private IProductRepository productRepository;

  @Transactional
  @Override
  public Product save(Product obj) {
    return productRepository.save(obj);
  }

  @Transactional
  @Override
  public Product modify(Product obj) {
    return productRepository.save(obj);
  }

  @Override
  public List<Product> listAll() {
    return productRepository.findAll();
  }

  @Override
  public Product findById(Integer id) {
    return productRepository.findById(id).orElse(new Product());
  }

  @Transactional
  @Override
  public void remove(Integer id) {
      productRepository.deleteById(id);
  }

}
