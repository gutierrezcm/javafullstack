package com.mitocode.javafullstack.service.impl;

import com.mitocode.javafullstack.model.Sale;
import com.mitocode.javafullstack.repository.IDetailSaleRepository;
import com.mitocode.javafullstack.repository.ISaleRepository;
import com.mitocode.javafullstack.service.ISaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SaleServiceImpl implements ISaleService {

  @Autowired
  private ISaleRepository saleRepository;

  @Autowired
  private IDetailSaleRepository detailSaleRepository;

  @Transactional
  @Override
  public Sale save(Sale sale) {
    sale.getDetailsSale().forEach(detailSale -> detailSale.setSale(sale));
    return saleRepository.save(sale);
  }

  @Transactional
  @Override
  public Sale modify(Sale obj) {
    return saleRepository.save(obj);
  }

  @Override
  public List<Sale> listAll() {
    return saleRepository.findAll();
  }

  @Override
  public Sale findById(Integer id) {
    return saleRepository.findById(id).orElse(new Sale());
  }

  @Transactional
  @Override
  public void remove(Integer id) {
    saleRepository.deleteById(id);
  }

}
