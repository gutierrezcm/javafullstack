package com.mitocode.javafullstack.service.impl;

import com.mitocode.javafullstack.model.Person;
import com.mitocode.javafullstack.repository.IPersonRepository;
import com.mitocode.javafullstack.service.IPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PersonServiceImpl implements IPersonService {

  @Autowired
  private IPersonRepository personRepository;

  @Transactional
  @Override
  public Person save(Person obj) {
    return personRepository.save(obj);
  }

  @Transactional
  @Override
  public Person modify(Person obj) {
    return personRepository.save(obj);
  }

  @Override
  public List<Person> listAll() {
    return personRepository.findAll();
  }

  @Override
  public Person findById(Integer id) {
     return personRepository.findById(id).orElse(new Person());
  }

  @Transactional
  @Override
  public void remove(Integer id) {
      personRepository.deleteById(id);
  }

}
