package com.mitocode.javafullstack.service;

import java.util.List;

public interface ICrud<T, V> {

  T save(T obj);
  T modify(T obj);
  List<T> listAll();
  T findById(V id);
  void remove(V id);

}
