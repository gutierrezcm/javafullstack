package com.mitocode.javafullstack.service;

import com.mitocode.javafullstack.model.Sale;

public interface ISaleService extends ICrud<Sale, Integer> {

}
