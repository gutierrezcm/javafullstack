package com.mitocode.javafullstack.service;

import com.mitocode.javafullstack.model.Product;

public interface IProductService extends ICrud<Product, Integer> {
}
