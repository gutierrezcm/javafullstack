package com.mitocode.javafullstack.service;

import com.mitocode.javafullstack.model.Person;

public interface IPersonService extends ICrud<Person, Integer> {
}
